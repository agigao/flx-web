* Flx assignment (web page)

  Web page to connect to flx api and check if second string can be composed using characters of the first one.

** build page and run figwheel repl
#+BEGIN_SRC bash
    clojure -M:fig:build
#+END_SRC

  host and port for api connection is hardcoded: /localhost:8080/

Cheers,\\
Giga
