(ns ^:figwheel-hooks gig.flx
  (:require
   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom]]
   [reagent.dom :as rdom]
   [ajax.core :refer [GET]]))

(defn get-app-element []
  (gdom/getElement "app"))

(defn handler [uri state]
  (let [s1 (:s1 @state)
        s2 (:s2 @state)
        query (str uri "/" s1 "/" s2)]
    (js/console.log "URL: " query)
    (GET query
         {:handler (fn [response]
                     (swap! state assoc :result response))})))

(def api-server "http://localhost:8080")

(def state (atom {:s1 "sators"
                  :s2 "rotass"
                  :result ""}))

(defn form [s1 s2]
  (fn []
    [:div
     [:form {:on-submit (fn [e]
                          (.preventDefault e)
                          (handler api-server state))}
      [:div.tmp "[debug] State:" (pr-str @state)]
      [:input {:name :s1
               :value (:s1 @state)
               :on-change (fn [e]
                            (swap! state assoc :s1 (-> e .-target .-value)))}]
      [:input {:name :s2
               :value (:s2 @state)
               :on-change (fn [e]
                            (swap! state assoc :s2 (-> e .-target .-value)))}]
      [:button {:type :submit} "Scramble?"]
      [:div "result: " (:result @state)]]]))

(defn mount [el]
  (rdom/render [form] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element))
